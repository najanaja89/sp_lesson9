﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sp_lesson9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Parallel.Invoke(
            //    () =>
            //    {
            //        Console.WriteLine($"({Thread.CurrentThread.ManagedThreadId})-1");
            //    },
            //    () =>
            //    {
            //        Console.WriteLine($"({Thread.CurrentThread.ManagedThreadId})-2");
            //    },
            //    () =>
            //    {
            //        Console.WriteLine($"({Thread.CurrentThread.ManagedThreadId})-3");
            //    });

            Console.WriteLine("Std");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i + " ");
            }

            Console.WriteLine("\nParalell");
            Parallel.For(0, 10, (i) =>
            {
                Console.WriteLine(i + $" ({Thread.CurrentThread.ManagedThreadId})");
            });

            Console.WriteLine("\nParallel ForEach");
            Parallel.ForEach(new int[] { 1, 2, 3, 4 }, (number) => { Console.WriteLine(number + $" ({ Thread.CurrentThread.ManagedThreadId})"); }); //For работает быстрее

            var users = new List<User>
            {
                new User
                {
                    Login="Karina Sultanova",
                    Password="123"
                },
                  new User
                {
                    Login="Barry Allen",
                    Password="1234"
                },
                    new User
                {
                    Login="Peter Parker",
                    Password="345"
                },
                      new User
                {
                    Login="Bruce Wayne",
                    Password="678"
                },
                        new User
                {
                    Login="John Wick",
                    Password="876"
                },
                          new User
                {
                    Login="Ruslan Tyo",
                    Password="3566"
                }
            };

            List<object> longResult = new List<object>();
            foreach (var user in users)
            {
                if (user.Login.ToLower().Contains("ad"))
                {
                    longResult.Add(new { Name = user.Login });
                }
            }

            var result = from user in users
                         where user.Login.ToLower().Contains("ad")
                         select new { Name = user.Login };

            var shortResult = users.Where(user => user
            .Login
            .ToLower()
            .Contains("ad"))
            .Select(user => new { Name = user.Login });

            var resultParallelLINQ = from user in users.AsParallel()
                         where user.Login.ToLower().Contains("ad")
                         select new { Name = user.Login };

            var shortResultParallelLINQ = users.AsParallel()
                .Where(user => user.Login.ToLower()
                .Contains("ad"))
                .Select(user => new { Name = user.Login });

            Console.ReadLine();
        }
    }
}
